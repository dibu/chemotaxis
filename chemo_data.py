#!/opt/local/bin/python3 --

import numpy as np
from matplotlib import pyplot as plt


class ChemoData:
  def __init__(self, individuals='individuals.out'):
    raw_data = np.loadtxt(individuals)

    self.x_coordinates = raw_data[:,1]
    self.y_coordinates = raw_data[:,2]
    self.concentration = raw_data[:,4]

    self.relative_vectors = self.compute_relative_vectors(
        self.x_coordinates,
        self.y_coordinates)
    self.x_axis_angles = self.compute_angles_from(self.relative_vectors)
    self.angle_change  = self.compute_angle_change_from(self.x_axis_angles)

    self.concentration_change = self.concentration[1:] - self.concentration[:-1]

    return

  @staticmethod
  def compute_relative_vectors(x_coords, y_coords):
    """
    Compute vectors that point from (x_i, y_i) to (x_{i+1}, y_{i+1})

    In:
      x_coords(np.array): (n_points,) array of x-coordinates
      y_coords(np.array): (n_points,) array of y-coordinates
    Out:
      vectors(np.array): (2,n_points-1) array of vectors
    """
    if x_coords.ndim != 1:
      raise ValueError("x_coords argument's dimension is not 1")
    if y_coords.ndim != 1:
      raise ValueError("y_coords argument's dimension is not 1")
    if x_coords.size != y_coords.size:
      raise ValueError("x_coords and y_coords sizes are not equal")

    n_points = x_coords.size
    vectors = np.empty( (2, n_points-1) )

    vectors[0] = x_coords[1:] - x_coords[:-1]
    vectors[1] = y_coords[1:] - y_coords[:-1]

    return vectors

  @staticmethod
  def compute_angles_from(vectors):
    """
    Compute vectors' angles with x-axis

    In:
      vectors(np.array): (2,n_vectors) array of vectors
    In:
      x_axis_angles(np.array): (n_vectors,) array of angles
    """
    return np.arctan2(vectors[1], vectors[0])

  @staticmethod
  def compute_angle_change_from(angles):
    """
    Compute Δ-angles from angles: angle2 - angle1

    In:
      angles(np.array): (n_angles,) array of angles, [-pi, pi)
    Out:
      (np.array): (n_angles-1,) array of angles, [-pi, pi)
    """
    abs_change  = np.abs(angles[1:] - angles[:-1])
    sign_change = np.sign(angles[1:] - angles[:-1])
    magnitude = np.minimum.reduce( [abs_change, 2*np.pi - abs_change] )
    return np.multiply(sign_change, magnitude)


