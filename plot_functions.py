#!/opt/local/bin/python3 --

import numpy as np
from matplotlib import pyplot as plt


def plot_angle_change_histogram(chemodata):
  """
  Plot histogram of the angle change

  In:
    chemodata(ChemoData): object with chemotaxis data, angles etc.
  Out:
    None
  """
  plt.hist(chemodata.angle_change, bins=100)
  return


def plot_conditional_angle_change_histogram(chemodata):
  mask_where_positive = chemodata.concentration_change[:-1] > 0
  mask_where_negative = chemodata.concentration_change[:-1] < 0

  angle_change_conditioned_on_positive = chemodata.angle_change[mask_where_positive]
  angle_change_conditioned_on_negative = chemodata.angle_change[mask_where_negative]

  plt.hist(
      angle_change_conditioned_on_positive,
      bins=100, density=True, alpha=0.5, label='cond. on positive')

  plt.hist(
      angle_change_conditioned_on_negative,
      bins=100, density=True, alpha=0.5, label='cond. on negative')

  plt.legend()
  plt.tight_layout()


def plot_conditional_magnitude_angle_change_histogram(chemodata):
  mask_where_positive_large = chemodata.concentration_change[:-1] > 3.5e-6
  mask_where_positive_small = (
      (chemodata.concentration_change[:-1] < 3.5e-6) &
      (chemodata.concentration_change[:-1] > 0))
  mask_where_negative_large = chemodata.concentration_change[:-1] < -3.5e-6
  mask_where_negative_small = (
      (chemodata.concentration_change[:-1] > -3.5e-6) &
      (chemodata.concentration_change[:-1] < 0))

  angle_change_conditioned_on_positive_large = (
      chemodata.angle_change[mask_where_positive_large])
  angle_change_conditioned_on_positive_small = (
      chemodata.angle_change[mask_where_positive_small])
  angle_change_conditioned_on_negative_large = (
      chemodata.angle_change[mask_where_negative_large])
  angle_change_conditioned_on_negative_small = (
      chemodata.angle_change[mask_where_negative_small])

  plt.hist(
      angle_change_conditioned_on_positive_large,
      bins=100, density=True, alpha=0.5, label='cond. on positive large')

  plt.hist(
      angle_change_conditioned_on_negative_large,
      bins=100, density=True, alpha=0.5, label='cond. on negative large')

  plt.legend()
  plt.tight_layout()


